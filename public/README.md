## About
Students' projects from Game Development@Telerik Academy School. Learn more about Telerik Academy School at https://www.telerikacademy.com/school.   

## Portfolio
The portfolio contains all the examples, projects and workshops used in Game Development@Telerik Academy School. The examples are intended for students and teachers. Check the examples at https://telerikacademyschool.gitlab.io/webdev.  

## Moodle
If you are already a student, check your course at https://school.telerikacademy.com.  

> Contact vyara@telerikacademy.com for assistance. 