 function search() {
         let input, filter, namelist, examples
         input = document.getElementById("input")
         filter = input.value.toUpperCase()
         namelist = document.getElementById("examples-list")
         examples = namelist.getElementsByTagName("a")
         for (let i = 0; i < examples.length; i++) {
            if (examples[i].innerHTML.toUpperCase().indexOf(filter) > -1)
               examples[i].style.display = ""
            else
               examples[i].style.display = "none"
         }
      }